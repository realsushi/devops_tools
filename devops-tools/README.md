### Devops tools

#### Des outils pour Python afin de mettre le code Python en micro-service
Voici plusieurs cas d'usage afin de développer avec Docker, de cette façon on peut faire un cluster Kubernetes sans souci de dépendance ou d'environnement.

#### Problème/Avantage avec Python
A l'inverse de NodeJS, aucun pacage manager n'est imposé, il faut installer pip à part.
A l'inverse de NodeJS, le package manager n'a pas de Scope, par défaut on installe tout en global.

- Problèmes
1.  Plusieurs packages manager dispo avec des libs différentes (pip/conda)
2.  Le scope de chaque code source n'est pas explicite. On ne sait pas si on doit utiliser Venv ou Conda ou Pyenv...
3.  On est tenté de faire `sudo pip install`
4.  En conclusion ça alourdit la documentation
5.  Rien ne garantit que ça va marcher sur une autre machine

- Avantages
1. le fait de ne pas imposer rend le code plus flexible
2. NPM est rempli de bugs complexes à résoudre lors du `npm install` en comparaison
3. Pas de dossier node_modules avec ownership à gerer (arrive souvent si on lance docker en root ou si on fait `sudo npm install`)

#### Solution rapide: python shell
On copie colle le snippet dans un shell python3 et on espère que ça marche.

#### Solution intermédiare: Jupyter Notebook
Le notebook tourne isolé sur son propre "Kernel". Nota: on peut aussi faire du R, du Julia et du Jshell(Java)... Cependant, on est bloqué dans une architecture monolithique, la gestion du filesystem est plus complexe.

#### La solution:
On monte le code dans un environnement Docker. 3 stratégies sont possibles:
- `docker run` pas de montage du filesystem, on copie colle le code à la main dans une interface(terminal/notebook/autre)
- `docker copy` on créé un snapshot du code dans le docker. On travaille sans filet, pas de sauvegarde facile.
- `docker run -v HOST:GUEST` on monte le filesystem dans le docker. Cela peut être en Read+Write (bilatéral) ou en RO(unilatéral du dev vers le container)

**Best practice:** On pense à lister ses dépendances dans un fichier `requirements.txt`

#### Uses case Python DEV
1. Le snipept Notebook (le fameux)
    - `docker run -it --name notebook1 --network host jupyter/datascience-notebook`
    On peut changer l'image si besoin mais il faudra modifier la ligne de commande (pas forcément pratique pour les non initiés au culte de la divinité Docker)

2. Mise en "Production" micro-service
    - `docker build -t pythonprod1 -f prod/PythonProd.Dockerfile`
    Si le build passe, on peut reprendre sa respiration. Si le Run passe on peut prendre un café. Dans la pratique on met à jour le Dockerfile pour avoir le moins d'options à taper dans le shell (risque d'oubli).
    Point important, on met à jour les dépendances.
    On essaie de spécifier la version du docker utilisée, ie `FROM python:3.9`
    - `docker run -it --name notebook1 --network host pythonprod1` la première execution - c'est assez pratique de mettre un nom que l'on incrémente ici.
    - `docker exec -it notebook1` après le reboot... ou autre
    - `docker logs notebook1` si besoin

3. Le docker-compose (pour éviter de rentrer les paramètres à la mano)
    - `docker-compose up --build`
    Le code est monté.
    On choisit l'image en modifiant le `docker-compose.yml`: soit on prend un docker hosté sur dockerhub, soit on prend un dockerfile, soit on met un token pour faire un docker login **registry.gitlab.com** (gitlab simplifie la vie)


**Conclusion**, plus on passe de temps à configurer le docker/Dockerfile/docker-compose plus on va passer à l'étape du micro-service clusterisé facilement (k8s)

- `docker login https:registry.gitlab.com`
- `docker tag blabla [todo]  mytag1`
- `docker push mytag1`